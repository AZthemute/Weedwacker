﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.ChangeAvatarReq)]
	public static async Task HandleChangeAvatarReq(Connection session, byte[] header, byte[] payload)
	{
		ChangeAvatarReq req = ChangeAvatarReq.Parser.ParseFrom(payload);
		await session.Player.TeamManager.ChangeAvatar(req.Guid);
		await session.SendPacketAsync(new PacketChangeAvatarRsp(req.Guid));
	}
}

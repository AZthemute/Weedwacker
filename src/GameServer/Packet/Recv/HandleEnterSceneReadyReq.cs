﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.EnterSceneReadyReq)]
	public static async Task HandleEnterSceneReadyReq(Connection session, byte[] header, byte[] payload)
	{
		EnterSceneReadyReq proto = EnterSceneReadyReq.Parser.ParseFrom(payload);
		// Reject player if invalid token
		if (session.Player.EnterSceneToken == proto.EnterSceneToken)
		{
			await session.Player.World.BroadcastPacketAsync(new PacketEnterScenePeerNotify(session.Player));

		}
		await session.SendPacketAsync(new PacketEnterSceneReadyRsp(session.Player, proto.EnterSceneToken));
	}
}

﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.PlayerLoginReq)]
	public static async Task HandlePlayerLoginReq(Connection session, byte[] header, byte[] payload)
	{
		// Check
		if (session.Player == null)
		{
			session.Stop();
			return;
		}

		// Parse request
		PlayerLoginReq req = PlayerLoginReq.Parser.ParseFrom(payload);

		// Authenticate session
		if (req.Token != session.Player.Token)
		{
			session.Stop();
			return;
		}

		// Login Setup
		await session.Player.OnLoginAsync();


		// Final packet to tell client logging in is done
		await session.SendPacketAsync(new PacketPlayerLoginRsp(session));
	}
}

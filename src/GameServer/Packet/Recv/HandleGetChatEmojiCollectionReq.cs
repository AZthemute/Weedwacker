﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetChatEmojiCollectionReq)]
	public static async Task HandleGetChatEmojiCollectionReq(Connection session, byte[] header, byte[] payload)
	{
		await session.SendPacketAsync(new PacketGetChatEmojiCollectionRsp(session.Player));
	}
}

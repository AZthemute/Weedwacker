﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.GetWidgetSlotReq)]
	public static async Task HandleGetWidgetSlotReq(Connection session, byte[] header, byte[] payload)
	{
		await session.SendPacketAsync(new PacketGetWidgetSlotRsp(session.Player));
	}
}

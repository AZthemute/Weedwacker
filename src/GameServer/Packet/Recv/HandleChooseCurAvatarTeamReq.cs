﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.ChooseCurAvatarTeamReq)]
	public static async Task HandleChooseCurAvatarTeamReq(Connection session, byte[] header, byte[] payload)
	{
		ChooseCurAvatarTeamReq p = ChooseCurAvatarTeamReq.Parser.ParseFrom(payload);

		await session.Player.TeamManager.SetCurrentTeam(p.TeamId);

		await session.Player.SendPacketAsync(new PacketChooseCurAvatarTeamRsp(p.TeamId));
	}
}

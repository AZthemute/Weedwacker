﻿using Weedwacker.GameServer.Enums;
using Weedwacker.GameServer.Packet.Send;
using Weedwacker.Shared.Network.Proto;

namespace Weedwacker.GameServer.Packet.Recv;

internal static partial class PacketHandler
{
	[OpCode((ushort)OpCode.EntityAiSyncNotify)]
	public static async Task HandleEntityAiSyncNotify(Connection session, byte[] header, byte[] payload)
	{
		EntityAiSyncNotify req = EntityAiSyncNotify.Parser.ParseFrom(payload);

		if (req.LocalAvatarAlertedMonsterList.Count > 0)
		{
			await session.Player.Scene.BroadcastPacketAsync(new PacketEntityAiSyncNotify(req));
		}
	}
}

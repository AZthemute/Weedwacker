﻿using System.Numerics;
using System.Text.RegularExpressions;
using NLua;
using Weedwacker.GameServer.Data.Enums;

namespace Weedwacker.GameServer.Systems.Script.Scene;

internal partial class SceneInfo
{
		private Lua LuaState;
	private int SceneId;
	public SceneConfig? scene_config;

	public SortedList<int, uint>? blocks; // <index ,blockIds>
	public SortedList<uint, SceneBlock>? BlocksInfo; // blockId
	public SortedList<int, Rectangle>? block_rects = new(); // <index, rectangle>
	public DummyPoints? dummy_points; // load dummy points from Scene<sceneId>_dummy_points.lua
	public LuaTable routes_config;// => LuaState.GetTable("routes_config"); // load routes from ???

	public static Task<SceneInfo> CreateAsync(Lua lua, uint sceneId, string scriptPath)
	{
		SceneInfo? scene = new();
		return scene.InitializeAsync(lua, sceneId, scriptPath);
	}

	private async Task<SceneInfo> InitializeAsync(Lua lua, uint sceneId, string scriptPath)
	{
		LuaState = lua;
		SceneId = SceneId;
		LuaState.LoadCLRPackage();
		LuaState.DoString(@" import ('GameServer', 'Weedwacker.GameServer.Systems.Script')");
		LuaRegistrationHelper.Enumeration<AnimatorParamType>(LuaState);
		LuaRegistrationHelper.Enumeration<ControlPartForwardBy>(LuaState);
		LuaRegistrationHelper.Enumeration<ControlPartRotateBy>(LuaState);
		LuaRegistrationHelper.Enumeration<ControlPartDoOnUnEnabled>(LuaState);
		LuaRegistrationHelper.Enumeration<DropElemControlType>(LuaState);
		LuaRegistrationHelper.Enumeration<EndureType>(LuaState);
		LuaRegistrationHelper.Enumeration<EntityType>(LuaState);		
		LuaRegistrationHelper.Enumeration<EventType>(LuaState);
		LuaRegistrationHelper.Enumeration<GadgetState>(LuaState);
		LuaRegistrationHelper.Enumeration<GadgetType>(LuaState);
		LuaRegistrationHelper.Enumeration<GearType>(LuaState);
		LuaRegistrationHelper.Enumeration<GroupKillPolicy>(LuaState);		
		LuaRegistrationHelper.Enumeration<ParentQuestState>(LuaState);
		LuaRegistrationHelper.Enumeration<PlatformRotType>(LuaState);
		LuaRegistrationHelper.Enumeration<PickType>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestCondType>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestContentType>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestExecType>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestGuideAuto>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestGuideLayer>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestGuideStyle>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestGuideType>(LuaState);		
		LuaRegistrationHelper.Enumeration<QuestShowType>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestState>(LuaState);
		LuaRegistrationHelper.Enumeration<QuestType>(LuaState);
		LuaRegistrationHelper.Enumeration<RandomQuestFilterType>(LuaState);
		LuaRegistrationHelper.Enumeration<RegionShape>(LuaState);
		LuaRegistrationHelper.Enumeration<ShowQuestGuideType>(LuaState);
		LuaRegistrationHelper.Enumeration<TalkBeginWay>(LuaState);
		LuaRegistrationHelper.Enumeration<TalkHeroType>(LuaState);
		LuaRegistrationHelper.Enumeration<TalkRoleType>(LuaState);
		LuaRegistrationHelper.Enumeration<TalkShowType>(LuaState);		
		LuaRegistrationHelper.Enumeration<VisionLevelType>(LuaState);
		lua.DoString(@"local function searcher(module_name)
    -- Use ""/"" instead of ""."" as directory separator
    local path, err = package.searchpath(module_name, package.path, ""/"")
    if path then
        return assert(loadfile(path))
    end
    return err
end");
		lua.DoString(@"table.insert(package.searchers, searcher)");
		lua.DoString($"_SCENE{sceneId} = {{}}");
		FileInfo sceneInfo = new(Path.Combine(scriptPath, "Scene", $"{sceneId}", $"scene{sceneId}.lua"));
		string sceneScript = sceneInfo.FullName;
		sceneScript = EscapeRegex().Replace(sceneScript, @"\\"); // replace \\ with \\\\
		lua.DoString("loadScene = loadfile (\"" + sceneScript + "\"" + $", \"bt\" , _SCENE{sceneId})");
		lua.DoString("loadScene()");
		if (lua[$"_SCENE{sceneId}.{nameof(scene_config)}"] != null)
			scene_config = new SceneConfig(lua.GetTable($"_SCENE{sceneId}.{nameof(scene_config)}"));
		if (lua[$"_SCENE{sceneId}.{nameof(blocks)}"] != null)
		{
			LuaTable? bloks = lua.GetTable($"_SCENE{sceneId}.{nameof(blocks)}");
			blocks = new SortedList<int, uint>(lua.GetTableDict(bloks).ToDictionary(w => (int)(long)w.Key, w => (uint)(long)w.Value));
			BlocksInfo = new SortedList<uint, SceneBlock>();
			List<Task>? tasks = new();
			foreach (uint blockId in blocks.Values)
			{
				tasks.Add(AddBlock(sceneId, blockId, Path.Combine(scriptPath, "Scene", $"{sceneId}")));
			}
			await Task.WhenAll(tasks);
		}
		if (lua[$"_SCENE{sceneId}.{nameof(block_rects)}"] != null)
		{
			LuaTable? rects = lua.GetTable($"_SCENE{sceneId}.{nameof(block_rects)}");
			Dictionary<object, object>? rectDict = lua.GetTableDict(rects);
			block_rects = new SortedList<int, Rectangle>(rectDict.ToDictionary(w => (int)(long)w.Key, w => new Rectangle(w.Value as LuaTable)));
		}
		if (lua[$"_SCENE{sceneId}.{nameof(dummy_points)}"] != null)
		{
			dummy_points = DummyPoints.Create(lua, sceneId, Path.Combine(scriptPath, "Scene", $"{sceneId}", $"scene{sceneId}_{nameof(dummy_points)}.lua"));
		}

		return this;
	}

	private async Task AddBlock(uint sceneId, uint blockId, string path)
	{
		SceneBlock? block = await SceneBlock.CreateAsync(LuaState, sceneId, blockId, path);
		BlocksInfo.Add(blockId, block);
	}

	public class SceneConfig
	{
		private LuaTable Table;
		// ONLY X,Z
		public Vector3 begin_pos => new((float?)(double?)Table[$"{nameof(begin_pos)}.x"] ?? 0, default, (float?)(double?)Table[$"{nameof(begin_pos)}.z"] ?? 0);
		// ONLY X,Z
		public Vector3 size => new((float?)(double?)Table[$"{nameof(size)}.x"] ?? 0, default, (float?)(double?)Table[$"{nameof(begin_pos)}.z"] ?? 0);
		public Vector3 born_pos => new((float?)(double?)Table[$"{nameof(born_pos)}.x"] ?? 0, (float?)(double?)Table[$"{nameof(born_pos)}.y"] ?? 0, (float?)(double?)Table[$"{nameof(born_pos)}.z"] ?? 0);
		public Vector3 born_rot => new((float?)(double?)Table[$"{nameof(born_rot)}.x"] ?? 0, (float?)(double?)Table[$"{nameof(born_rot)}.y"] ?? 0, (float?)(double?)Table[$"{nameof(born_rot)}.z"] ?? 0);
		public float? die_y => (int?)(long?)Table[$"die_y"];
		public RoomInfo room_safe_pos;
		// ONLY X,Z
		public Vector3 vision_anchor => new((float?)(double?)Table[$"{nameof(vision_anchor)}.x"] ?? 0, default, (float?)(double?)Table[$"{nameof(vision_anchor)}.z"] ?? 0);

		public SceneConfig(LuaTable table)
		{
			Table = table;
			room_safe_pos = new RoomInfo(Table);
		}
	}

	public class Rectangle
	{
		private LuaTable Table;
		// ONLY X,Z
		public Vector3 min => new((float?)(double?)Table[$"{nameof(min)}.x"] ?? 0, default, (float?)(double?)Table[$"{nameof(min)}.z"] ?? 0);
		// ONLY X,Z
		public Vector3 max => new((float?)(double?)Table[$"{nameof(max)}.x"] ?? 0, default, (float?)(double?)Table[$"{nameof(max)}.z"] ?? 0);

		public Rectangle(LuaTable table)
		{
			Table = table;
		}
	}

	public class RoomInfo
	{
		private LuaTable Table;
		public int? scene_id => (int?)(long?)Table[$"{nameof(scene_id)}"] ?? 0;
		public Vector3 safe_pos => new((float?)(double?)Table[$"{nameof(safe_pos)}.x"] ?? 0, (float?)(double?)Table[$"{nameof(safe_pos)}.y"] ?? 0, (float?)(double?)Table[$"{nameof(safe_pos)}.z"] ?? 0);
		public Vector3 safe_rot => new((float?)(double?)Table[$"{nameof(safe_rot)}.x"] ?? 0, (float?)(double?)Table[$"{nameof(safe_rot)}.y"] ?? 0, (float?)(double?)Table[$"{nameof(safe_rot)}.z"] ?? 0);

		public RoomInfo(LuaTable table)
		{
			Table = table[$"{nameof(SceneConfig.room_safe_pos)}"] as LuaTable;
		}
	}

	[GeneratedRegex("(?<!\\\\)[\\\\](?!\\\\)")]
	private static partial Regex EscapeRegex();
}

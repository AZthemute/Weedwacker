﻿using Weedwacker.GameServer.Data;
using Weedwacker.GameServer.Data.BinOut.Ability.Temp;
using Weedwacker.GameServer.Data.BinOut.Gadget;
using Weedwacker.GameServer.Data.BinOut.Shared.ConfigEntity;
using Weedwacker.GameServer.Systems.World;
using Weedwacker.Shared.Utils;

namespace Weedwacker.GameServer.Systems.Ability
{
    internal class GadgetAbilityManager : BaseAbilityManager
    {
        private BaseGadgetEntity GadgetOwner => Owner as BaseGadgetEntity;
        private ConfigGadget Config => GameData.ConfigGadgetMap[GadgetOwner.Data.jsonName];
        private ConfigGadget? ItemConfig => GameData.ConfigGadgetMap.GetValueOrDefault(GadgetOwner.Data.itemJsonName, null);
        public override Dictionary<uint, Dictionary<uint, object>?>? AbilitySpecials { get; } = new();
        public override Dictionary<string, HashSet<string>> UnlockedTalentParams => throw new NotImplementedException();
        protected override Dictionary<uint, ConfigAbility> ConfigAbilityHashMap { get; } = new();

        public GadgetAbilityManager(BaseGadgetEntity owner) : base(owner)
        {
        }

        public override void Initialize()
        {
            if (Config.abilities != null)
            {
                foreach (ConfigEntityAbilityEntry abilityEntry in Config.abilities)
                {
                    ConfigAbility ability = GameData.ConfigAbilityMap[abilityEntry.abilityName];
                    uint abilityHash = Utils.AbilityHash(abilityEntry.abilityName);
                    ConfigAbilityHashMap.Add(abilityHash, ability);
                    if(ability.abilitySpecials != null)
                    {
                        AbilitySpecials[abilityHash] = new Dictionary<uint, object>();
                        foreach(KeyValuePair<string, object> special in ability.abilitySpecials)
                        {
                            AbilitySpecials[abilityHash][Utils.AbilityHash(special.Key)] = special.Value;
                        }
                    }
                }
            }

            if(ItemConfig != null && ItemConfig.abilities != null)
            {
                foreach (ConfigEntityAbilityEntry abilityEntry in ItemConfig.abilities)
                {
                    ConfigAbility ability = GameData.ConfigAbilityMap[abilityEntry.abilityName];
                    uint abilityHash = Utils.AbilityHash(abilityEntry.abilityName);
                    ConfigAbilityHashMap.Add(abilityHash, ability);
                    if (ability.abilitySpecials != null)
                    {
                        AbilitySpecials[abilityHash] = new Dictionary<uint, object>();
                        foreach (KeyValuePair<string, object> special in ability.abilitySpecials)
                        {
                            AbilitySpecials[abilityHash][Utils.AbilityHash(special.Key)] = special.Value;
                        }
                    }
                }
            }

            base.Initialize();
        }
    }
}

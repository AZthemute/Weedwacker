﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Weedwacker.SourceGenerator
{
	[Generator]
	public class HandlerSourceGenerator : IIncrementalGenerator
	{
		public void Initialize(IncrementalGeneratorInitializationContext context)
		{
			if(Debugger.IsAttached)
			{
				Debugger.Launch();
			}
			IncrementalValueProvider<ImmutableArray<ISymbol>> handlers = context.SyntaxProvider.CreateSyntaxProvider(IsPacketHandler, GetMethodTypeOrNull)
					   .Where(type => type != null)
					   .Collect();

			context.RegisterSourceOutput(handlers, GenerateCode);
		}
		private static bool IsPacketHandler(
			SyntaxNode syntaxNode,
			CancellationToken cancellationToken)
		{
			if (!(syntaxNode is AttributeSyntax attribute))
				return false;

			string name = ExtractName(attribute.Name);

			return name == "OpCode" || name == "OpCodeAttribute";
		}
		private static string ExtractName(NameSyntax name)
		{
			string typeName = name.GetType().Name;
			switch (typeName)
			{
				case nameof(IdentifierNameSyntax): return (name as IdentifierNameSyntax).Identifier.Text;
				//case nameof(SimpleNameSyntax): return (name as SimpleNameSyntax).Identifier.Text;
				//case nameof(QualifiedNameSyntax): return (name as QualifiedNameSyntax).Right.Identifier.Text;
				default: return null;
			};
		}
		private static ISymbol GetMethodTypeOrNull(
			GeneratorSyntaxContext context,
			CancellationToken cancellationToken)
		{
			AttributeSyntax attributeSyntax = (AttributeSyntax)context.Node;

			// "attribute.Parent" is "AttributeListSyntax"
			// "attribute.Parent.Parent" is a C# fragment the attributes are applied to
			if (!(attributeSyntax.Parent?.Parent is MethodDeclarationSyntax methodDeclaration))
				return null;

			ISymbol symbol = context.SemanticModel.GetDeclaredSymbol(methodDeclaration);

			return symbol == null || !IsHandler(symbol) ? null : symbol;
		}

		private static bool IsHandler(ISymbol symbol)
		{
			ImmutableArray<AttributeData> attr = symbol.GetAttributes();
			return attr.Any(a => a.AttributeClass?.Name == "OpCodeAttribute");// &&
				//a.AttributeClass.ContainingNamespace.Name == "Packet" &&
				//a.AttributeClass.ContainingNamespace.ContainingNamespace.Name == "GameServer");
		}
		private static void GenerateCode(
			SourceProductionContext context,
			ImmutableArray<ISymbol> handlers)
		{
			if (handlers.IsDefaultOrEmpty) return;
			string source = $@"// <auto-generated/>
	using Google.Protobuf;
	using Weedwacker.GameServer.Enums;
	using Weedwacker.Shared.Network.Proto;
	using Weedwacker.Shared.Utils;

	namespace Weedwacker.GameServer.Packet.Recv;

	internal static partial class PacketHandler
	{{
		public delegate Task Handler(Connection session, byte[] header, byte[] payload);	
		public static Dictionary<ushort, Handler> Handlers = new()
			{{
	";

			StringBuilder sb = new StringBuilder(source);

			foreach (ISymbol method in handlers)
			{
				AttributeData attr = method.GetAttributes().First(w => w.AttributeClass.Name == "OpCodeAttribute");
				ushort opcode = (ushort)attr.ConstructorArguments.First().Value;
				sb.Append(CreateDelegate(opcode, method.Name));
			}


			sb.Append($@"
			}};
	}}
	");
			// Add the source code to the compilation
			context.AddSource($"PacketHandler.g.cs", sb.ToString());
		}

		private static string CreateDelegate(ushort opcode, string handlerName)
		{
			return $"			{{ {opcode}, {handlerName}}},\n";
		}

		public void Initialize(GeneratorInitializationContext context)
		{
		}
	}
}
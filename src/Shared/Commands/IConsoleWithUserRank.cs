﻿using System.CommandLine;
using Weedwacker.Shared.Enums;

namespace Weedwacker.Shared.Commands
{
    public interface IConsoleWithUserRank:IConsole
    {
        public UserRank rank { get; }
    }
}
